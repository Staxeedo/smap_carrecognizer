#Script for getting Car Type, image path in argument
from torch.autograd import Variable
from PIL import Image
from torchvision import transforms
import numpy as np
from torch.nn import functional as F
import torch.nn as nn
import pretrainedmodels
import pytorch_lightning as pl
import torch
model_name = 'inceptionresnetv2'


class CarTypeRecognizer(pl.LightningModule):

    def __init__(self, num_target_classes):
        super().__init__()
        self.model = pretrainedmodels.__dict__[model_name]()
        num_ftrs = self.model.last_linear.in_features
    
        self.model.fc = nn.Linear(num_ftrs, num_target_classes)
        self.acc = pl.metrics.Accuracy()
    def forward(self, x):
        return self.model(x)

import sys
img = sys.argv[1]
the_model = CarTypeRecognizer(4)
the_model.load_state_dict(torch.load(
    "models/modevSamplerMinb8_25f.pth",map_location=torch.device('cpu')))

# the_model= CarTypeRecognizer.load_from_checkpoint(checkpoint_path="/home/madr/CODES/smap_carrecognizer/models/modevSamplerMinb8_25f.ckpt",num_target_classes=4,map_location=torch.device('cpu')),


mean = np.array([0.485, 0.456, 0.406])
std = np.array([0.229, 0.224, 0.225])
data_transforms = {
    'train': transforms.Compose([
        transforms.RandomResizedCrop(299),
        transforms.RandomHorizontalFlip(),
        transforms.RandomVerticalFlip(),
        transforms.ToTensor(),
        transforms.Normalize(mean, std)
    ]),
    'val': transforms.Compose([
        transforms.Resize(256),
        transforms.CenterCrop(299),
        transforms.ToTensor(),
        transforms.Normalize(mean, std)
    ]),
}

loader = transforms.Compose([
    transforms.Resize(256),
    transforms.CenterCrop(299),
    transforms.ToTensor(),
    transforms.Normalize(mean, std)])


def image_loader(image_name):
    """load image, returns cuda tensor"""
    image = Image.open(image_name)
    image = loader(image).float()
    image = Variable(image, requires_grad=True)
    image = image.unsqueeze(0)
    return image.cpu()



myImg = image_loader(img)
the_model.eval()
device = torch.device("cpu")
the_model.to(device)
outputs = the_model(myImg)
_, preds = torch.max(outputs, 1)
value = torch.IntTensor.item(preds)
print(value)