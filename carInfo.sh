 #!/bin/bash          
tempSPZ=$(docker run -it --rm -v $(pwd):/data:ro openalpr -c eu -n 1 ./$1)
SPZ="$(echo $tempSPZ | cut -d' ' -f5)"
CARTYPE=$(/home/madr/miniconda3/bin/python ./getCarType.py ./$1)
echo $tempSPZ
/home/madr/miniconda3/bin/python ./addToRecord.py $1 $CARTYPE $SPZ