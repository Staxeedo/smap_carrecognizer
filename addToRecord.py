#Script for pushing data to database and creating annoted image; informations in arguments
import mysql.connector
import time
import datetime
import sys
def getcarType(i):
  switcher={
    0:'offroad',
    1:'personal',
    2:'truck',
    3:'van',
  }
  return switcher.get(i,"Invalid Car type")

img = sys.argv[1]
carType = getcarType(int(sys.argv[2]))
if(len(sys.argv)==4):
  spz = sys.argv[3]
else:
  spz="none"

ts = time.time()
timestamp = datetime.datetime.fromtimestamp(ts).strftime('%Y-%m-%d %H:%M:%S')

mydb = mysql.connector.connect(
  host="localhost",
  user="root",
  password="",
  database="car"
)

mycursor = mydb.cursor()

sql = "INSERT INTO record (carType, spz,tstamp) VALUES (%s, %s,%s)"
val = (carType,spz,timestamp)
mycursor.execute(sql, val)

mydb.commit()
select = "SELECT recordID FROM record WHERE (spz=\""+spz+"\" AND tstamp=\""+timestamp+"\")"
mycursor.execute(select)

recordID = mycursor.fetchone()

import os
dest = "/home/madr/CODES/cartype_webapp/uploads/"+carType+"/"+str(recordID[0])+".jpg"
os.rename("/home/madr/CODES/smap_carrecognizer/"+img, dest)

#create annoted img
from PIL import Image, ImageFont, ImageDraw
img = Image.open(dest)
font = ImageFont.truetype("/usr/share/fonts/dejavu/DejaVuSans.ttf",int((img.height/100)*5) )
draw = ImageDraw.Draw(img)
draw.rectangle(((0, 00), (img.width,(img.height/100)*12 )), fill="black")
draw.text((0,0), "SPZ:"+str(spz)+";Type:"+str(carType), (255,255,0), font=font)
img.save("/home/madr/CODES/cartype_webapp/uploads/"+carType+"/"+str(recordID[0])+"a.jpg")